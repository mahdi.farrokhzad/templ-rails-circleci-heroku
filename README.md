# README

## Scripts
- `run.sh` : Start Rails API on `http://localhost:3000`
- `exec.sh`: Run command inside API container e.g. `./exec.sh rails -v`
- To get a shell inside the API container run `./exec.sh bash`

## To reuse this boilerplate
- Clone this repository and remove its git history `rm -rf .git` and `git init`
- Create and push to a repo on Github
- Update dependencies `./exec.sh bundle update`
- Replace `myapp`, `MYAPP` and `Myapp` in all files with a suitable name
- Set up local database for development using `./exec.sh rails db:setup`
- Sign up to [Circle CI](https://circleci.com/) and connect to your GitHub account. Add project pointing to the cloned repository
- Sign up to [Heroku](https://heroku.com) and create an app
- Add `HEROKU_API_KEY` environent variables to Circle CI [as detailed here]
(https://circleci.com/docs/2.0/deployment-integrations/#heroku)
- Set `HEROKU_APP_NAME` environment variable in `/.circleci/config.yml` to your Heroku app name
