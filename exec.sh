#!/bin/sh
set -e

docker-compose run --rm -v "$(pwd)":/myapp-api web $@
