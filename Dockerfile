FROM ruby:2.5.0

#RUN apt-get update -qq && apt-get install -y build-essential libpq-dev nodejs
RUN mkdir /myapp-api
WORKDIR /myapp-api
COPY Gemfile Gemfile.lock ./
RUN bundle install
COPY . ./

EXPOSE 3000
CMD ["rails", "server", "-p", "4000", "-b", "0.0.0.0"]
