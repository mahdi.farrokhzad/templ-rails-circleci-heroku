# frozen_string_literal: true

require 'rails_helper'

describe VersionController do
  before { get 'version' }

  it 'return a 200 code' do
    expect(response).to be_success
  end

  it 'correctly outputs the app version number' do
    expect(response.body).to eq(MyappApi::Application::VERSION)
  end
end
