# 6. Heroku and AWS preferred options for running apps

Date: 2018-03-12

## Status

Accepted

## Context

We need to deploy and run our Rails API and front end static React site somewhere
- Needs to be simple
- Should scale if required
- Should support PostgreSQL
- Should integrate well with CircleCI

## Decision

We're going to default to Heroku where possible
- It has great support for Rails and static HTML sites
- The team has experience using it
- We can get up and running quickly
- Integrates well with Circle CI

If the need arises for another hosting platform we'll use AWS. This might be for specialist functionality (e.g. AWS Lambda) or for cross-functional requirements such as scalability

## Consequences

- Robin is going to set up a boilerplate Rails APP that deploys to Heroku via Cirle CI. We can clone this to get started
- Nick is going to look into setting up a similar boilerplate repo for the static front end site
- Robin is sad that Heroku is based around git deploys. It breaks some of his Continuous Delivery fundamentals ("Build your binaries once") but he'll get over it
