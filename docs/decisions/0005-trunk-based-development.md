# 5. Trunk based development

Date: 2018-03-12

## Status

Accepted

## Context

We need to decide upon a development flow
- That avoids merge conflicts wherever possible
- Leans towards early integration
- Is simple
- Enables Continuous Delivery

## Decision

We're going to use [Trunk based development](https://trunkbaseddevelopment.com/)
- It's statistically proven to improve performance
- It's simple, and reduces the need for messy merges

## Consequences

- Only commits to ```master``` will be deployed downstream
- We're going to allow pull requests, on the condition they are merged quickly. If a PR is open for longer than a day then we're not practising Continuous Integration and this will need to be addressed
