# 3. Ruby on Rails for backend APIs

Date: 2018-03-12

## Status

Accepted

## Context

We need to pick a tech stack for our initial backend APIs.
- It needs to be something the team knows, and we can get up and running with quickly
- We are currently optimizing for speed to market
- No immediate scalability concerns

## Decision

We're going to use Ruby on Rails.
- The team has good experience with it
- It's mature
- It will allows us to get up and running quickly
- Good developer ecosystem

We're going to use PostgresSQL for underlying data store
- It's mature

## Consequences

All new APIs will be written in RoR until a better alternative presents itself
