# 2. Use Circle CI for Continuous Delivery pipeline

Date: 2018-03-12

## Status

Accepted

## Context

We need to have our code automatically built, tested and deployed on commit

## Decision

We're going to use [Circle CI](https://circleci.com/)
- The new Workflows offering allows you to have CD pipelines
- The team have worked with it before
- It plays well with a range of languages and infrastructure offerings that we're likely to use
- The declarative format is nice

## Consequences

- Robin is going to set up a boilerplate Circle CI pipeline for a generic Ruby API to bootstrap our efforts
