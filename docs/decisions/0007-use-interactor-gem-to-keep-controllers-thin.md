# 7. Use interactor gem to keep controllers thin

Date: 2018-03-12

## Status

Accepted

## Context

- We want to keep our Rails controllers thin to make the codebase easy to work with
- We want to be able to test business logic separately from HTTP handling logic
- We want structure our Rails API in a manner that makes it easy to split out microservices in the future if required

## Decision

- We are going to use the interactor Gem to separate controller and business logic: https://github.com/collectiveidea/interactor
- Paul G has used it before with good effect on a previoud DV project

## Consequences

- For any non-trivial controller actions we will create an interactor
