# 4. React and redux for the front end

Date: 2018-03-12

## Status

Accepted

## Context

- The Myapp web front end will be very interactive.
- We need to use tech we can get out the door quickly
- Code needs to be easy to test

## Decision

We are going to use React for front end rendering, with Redux for state management
- Team has good knowledge
- Strong dev community
- Easy to recruit for
- Good tooling offering

The front end application will be in a separate repository to the back end. It will generate a static site
- Allows for reduced complexity in each repository
- Allows for simpler build and deployment model


## Consequences

- Will set up a new repository for the front end
- We'll use Stephen Taylor's ```reactivation``` React starter kit to get us up and running
- When the APIs have settled we'll look into contract testing, potentially using ```pact```
- User stories will naturally bridge multiple repositories, so developers will need to work closely together to complete stories

