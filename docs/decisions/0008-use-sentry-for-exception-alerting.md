# 8. Use Sentry for exception alerting

Date: 2018-03-12

## Status

Accepted

## Context

- We need to know if the application errors, both on the front end and the back end
- Don't want to hand roll our own. More important things to be spending our time on

## Decision

Use Sentry (https://sentry.io)
- Stephen T has used it before and reccomends

## Consequences

We'll play a story early on integrate Sentry for front and back end
