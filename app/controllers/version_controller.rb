# frozen_string_literal: true

class VersionController < ApplicationController
  def version
    render plain: MyappApi::Application::VERSION
  end
end
